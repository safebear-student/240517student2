package com.safebear.app;

import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;


/**
 * Created by Admin on 24/05/2017.
 */
public class BaseTest {

    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;
    FramesPageMainFrame framesPageMainFrame;

    @Before
    public void setUp() throws MalformedURLException {
//        driver = new ChromeDriver();
        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnitWithJs();

        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"),capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        utility = new Utils();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);
        framesPageMainFrame = new FramesPageMainFrame(driver);
        assertTrue(utility.navigateToWebsite(driver));
    }

    @After
    public void tearDown(){
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

}
