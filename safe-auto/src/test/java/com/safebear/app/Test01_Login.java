package com.safebear.app;

import org.junit.Test;
import pages.WelcomePage;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 24/05/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    //Step 1
    public void testLogin(){
    assertTrue(welcomePage.checkCorrectPage());

    //Step 2
    assertTrue(welcomePage.clickOnLogin(this.loginPage));

    //Step 3 Login
    assertTrue(loginPage.login(this.userPage,"testuser","testing"));

    }
}
