package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 24/05/2017.
 */
public class FramesPageMainFrame {
    WebDriver driver;

    public FramesPageMainFrame(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
}
